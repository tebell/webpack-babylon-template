import {
  Engine,
  Scene,
  FreeCamera,
  Vector3,
  HemisphericLight,
  Mesh
} from '@babylonjs/core';
import '@babylonjs/loaders';
// import '@babylonjs/inspector'
// import '@babylonjs/materials';
// import '@babylonjs/post-processes';
// import '@babylonjs/serializers';

// import * as GUI from '@babylonjs/gui';

export class Game {

  constructor() {
    console.log('Game instantiated.');
  }

  foo(message) {
    console.log('foo!');
    let test = 'es8'.padStart(10, 'x');
    console.log(test);


    // get the canvas DOM element
    let canvas = document.getElementById('renderCanvas');

    // load the 3D engine
    let engine = new Engine(canvas, true);

    // createScene function that creates and return the scene
    let createScene = function() {
      // create a basic BJS Scene object
      let scene = new Scene(engine);

      // create a FreeCamera, and set its position to (x:0, y:5, z:-10)
      let camera = new FreeCamera('camera1', new Vector3(0, 5, -10), scene);

      // target the camera to scene origin
      camera.setTarget(Vector3.Zero());

      // attach the camera to the canvas
      camera.attachControl(canvas, false);

      // create a basic light, aiming 0,1,0 - meaning, to the sky
      let light = new HemisphericLight('light1', new Vector3(0, 1, 0), scene);

      // create a built-in "sphere" shape; its constructor takes 6 params: name, segment, diameter, scene, updatable, sideOrientation
      let sphere = Mesh.CreateSphere('sphere1', 16, 2, scene);

      // move the sphere upward 1/2 of its height
      sphere.position.y = 1;

      // create a built-in "ground" shape;
      let ground = Mesh.CreateGround('ground1', 6, 6, 2, scene);

      // return the created scene
      return scene;
    }

    // call the createScene function
    let scene = createScene();

    // run the render loop
    engine.runRenderLoop(() => {
      scene.render();
    });

    // the canvas/window resize event handler
    window.addEventListener('resize', () => {
      engine.resize();
    });
  }

}
