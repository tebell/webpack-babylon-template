const path = require('path');

module.exports = {
  entry: [
    '@babel/polyfill',
    './src/index.js'
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.bundle.js'
  }
};

const rules = [{
  test: /\.m?js$/,
  exclude: '/(node_modules)/',
  use: {
    loader: 'babel-loader',
    options: {
      presets: [
        '@babel/preset-env',
        {
          // targets: {
          //   edge: "17",
          //   firefox: "60",
          //   chrome: "67",
          //   safari: "11.1",
          // },
          targets: "cover 99.5%",
          useBuiltIns: "usage",
      },
    ]
    }
  }

}];

// module: {
//   exports: {
//     entry: './src/index.js',
//     output: {
//       path: path.resolve(__dirname, 'dist'),
//       filename: 'app.bundle.js'
//     }
//   }
//
// }
module.rules = {rules}
